#!/usr/bin/env groovy

@Grab(group = 'cc.redberry', module = 'groovy', version = '1.1.9')
import cc.redberry.groovy.Redberry
// import cc.redberry.core.context.*
// import cc.redberry.core.indices.*
// import cc.redberry.core.tensor.*
// import cc.redberry.core.transformations.*
// import static cc.redberry.groovy.RedberryPhysics.*
import static cc.redberry.groovy.RedberryPhysics.UnitaryTrace
// import static cc.redberry.groovy.RedberryStatic.*
import static cc.redberry.groovy.RedberryStatic.defineMatrices
// import static cc.redberry.core.tensor.Tensors.*
import static cc.redberry.core.tensor.Tensors.setAntiSymmetric
// import static cc.redberry.core.indices.IndexType.*
import static cc.redberry.core.indices.IndexType.Matrix2
// import static cc.redberry.core.context.OutputFormat.*
  
use(Redberry){
    setAntiSymmetric 'f_ABC'
    defineMatrices 'T_A', Matrix2.matrix
    def subs = 'f^ABC = -2*I*(Tr[T^A*T^B*T^C]-Tr[T^A*T^C*T^B])'.t
    def expr = 'f^ABC'.t
    println subs >> expr
}
