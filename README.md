# trying-redberry

* http://redberry.cc/
* https://arxiv.org/pdf/1302.1219.pdf
* https://groovy.apache.org/download.html#osinstall
* http://redberry.cc/installation

```shell
sudo dnf install java-latest-openjdk java-latest-openjdk-devel
sudo snap install groovy --classic
cd ~/git/trying-redberry
```

<!-- ```shell -->
<!-- wget http://redberry.s3-website-eu-west-1.amazonaws.com/redberry-1.1.9-all-in-one.jar -->
<!-- ``` -->

```shell
./MyRedberryScript.groovy
```
